## Configuración de ProxySQL con Galera

### Estructura de los directorios

- infra: Configuración básica de infraestuctura. Galera y ProxySQL.
- schemas: Definición de los esquemas a usar en la práctica.
- scripts: Scripts de pruebas para demostración.


### Sumario de actividades

1. Instalamos las dependencias:

Basados en Debian:

```
   apt-get install -y mysql-client sysbench
```

Basados en Arch:

```
   pacman -S --no-confirm mariadb-clients sysbench
```

2. Lanzamos ProxySQL:

```
  cd infra;
  docker-compose up proxysql
```

3. Nos conectamos a la consola de administración:

```
   mysql -uradmin -pradmin -h127.0.0.1 -P6032
```

4. Seguimos los pasos del ponente en la consola de administración.

```
   FAMILIARIZARNOS CON LA CONSOLA
```

### Ejercicios

#### Iniciales

1. Crear los usuarios necesarios según la configuración de MySQL de este repo.
2. Crear los servidores necesarios según la configuración de MySQL de este repo.
3. Crear el usuario de monitorización necesario según la configuración de MySQL de este repo.
4. Comprobar que ProxySQL está sirviendo tráfico usando `sysbench`.

#### Finales

4. Configurar ProxySQL en un read/write split:
  - Comprobar las reglas de consultas.
  - Comprobar las estádisticas de las reglas y los digests que hemos generado.
  - Volver a comprobar usando sysbench.
5. Configurar ProxySQL con las siguientes reglas de consultas:
  - Denegar `INSERT` queries al usuario `eslibre` a la tabla `eslibre_org`.
  - Dirigir todos las queries `SELECT` al hostgroup `20`, salvo los dirigidos a `eslibre.Payments`.
  - Comprobar las reglas con las estádisticas y volver a comprobar las digests.
