### Connect

MySQL:
```
mysql -uradmin -pradmin -h127.0.0.1 -P6032
```
Mariadb:
```
mysql -uradmin -pradmin -h127.0.0.1 -P6032 --sigint-ignore
```

### Rules

```
SELECT rule_id,active,schemaname,match_pattern,destination_hostgroup,apply FROM mysql_query_rules;
SELECT rule_id,active,schemaname,match_pattern,match_digest,destination_hostgroup,apply FROM runtime_mysql_query_rules;
```

### Stats

```
SHOW CREATE TABLE stats.stats_mysql_query_digest\G
SELECT hostgroup,schemaname,username,digest,digest_text,count_star FROM stats_mysql_query_digest;
SELECT tommand,total_time_us, command_cnt FROM stats_mysql_commands_counters;
```

### Docker

Fancy `ps`:

```
docker ps --format 'table {{.ID}}\t{{.Image}}\t{{.Ports}}\t{{.Names}}'
```

### Full config

```
mysql -uradmin -pradmin -h127.0.0.1 -P6032 < infra/conf/proxysql/docker-config.sql
```

### Watch commands

```
watch -n1 'mysql --table -uadmin -padmin -h127.0.0.1 -P6032 -e "SELECT hostgroup_id,port,status,comment FROM runtime_mysql_servers"'
```

### Sysbench

Prepare:
```
sysbench --db-driver=mysql --mysql-user=root --mysql_password=root --mysql-db=sysbench --mysql-host=127.0.0.1 --mysql-port=6033 --tables=10 --table-size=2000 --threads=10 --time=0 --report-interval=1 --db-ps-mode=disable --mysql-ignore-errors="all" oltp_read_write prepare
```

Run:
```
sysbench --db-driver=mysql --mysql-user=root --mysql_password=root --mysql-db=sysbench --mysql-host=127.0.0.1 --mysql-port=6033 --tables=10 --table-size=2000 --threads=10 --time=0 --report-interval=1 --db-ps-mode=disable --mysql-ignore-errors="all" oltp_read_write run
```

No-trx:
```
sysbench --db-driver=mysql --mysql-user=root --mysql_password=root --mysql-db=sysbench --mysql-host=127.0.0.1 --mysql-port=6033 --tables=10 --table-size=2000 --threads=10 --time=0 --report-interval=1 --db-ps-mode=disable --skip-trx --mysql-ignore-errors="all" oltp_read_write run
```
