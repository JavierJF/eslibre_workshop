#!/bin/bash

infra_pwd=""
[[ $0 =~ ^.*/infra ]] && infra_pwd=$BASH_REMATCH
if [[ $infra_pwd ]]; then
    pushd $infra_pwd
fi

sudo -- sh -c 'for f in $(find . -name error.log); do > $f; done'
sudo -- sh -c 'for f in $(find . -name error.log); do cat $f; done'
