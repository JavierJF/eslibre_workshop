#!/bin/bash

infra_pwd=""
[[ $0 =~ ^.*/infra ]] && infra_pwd=$BASH_REMATCH
if [[ $infra_pwd ]]; then
    pushd $infra_pwd
fi

export galera_1_data_mnt=$(docker volume inspect infra_galera_1_data | jq --raw-output ".[].Mountpoint")
echo "Found mountpoint: "$galera_1_data_mnt". Forcing bootstraping..."
sudo -E sh -c 'echo "safe_to_bootstrap: 1" > $galera_1_data_mnt/grastate.dat'
echo "Edited 'grastate.dat' contents:"
echo ":: "$(sudo -E cat $galera_1_data_mnt/grastate.dat)
