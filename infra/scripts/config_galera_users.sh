#!/bin/bash

infra_pwd=""
[[ $0 =~ ^.*/infra ]] && infra_pwd=$BASH_REMATCH
if [[ $infra_pwd ]]; then
    pushd $infra_pwd
fi

for h in mysql_galera_1-1 mysql_galera_2-1 mysql_galera_3-1; do
printf "[$(date)] Waiting for MySQL service on $(basename $PWD)-$h"
  RC=1
  while [ $RC -eq 1 ]
  do
    sleep 1
    printf "."
    docker exec $(basename $PWD)-$h mysqladmin ping > /dev/null 2>&1
    RC=$?
  done
  printf "\n"
done

sleep 2

docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "CREATE DATABASE IF NOT EXISTS eslibre"

docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "CREATE USER IF NOT EXISTS root@'%' IDENTIFIED BY 'root'"
docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "GRANT ALL PRIVILEGES ON *.* TO root@'%'"

docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "CREATE USER IF NOT EXISTS eslibre@'%' IDENTIFIED BY 'eslibre'"
docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "GRANT ALL ON eslibre.* TO eslibre@'%'"
docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "GRANT ALL ON eslibre_admin.* TO eslibre@'%'"
docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "GRANT ALL ON simple_db.* TO eslibre@'%'"

docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "CREATE USER IF NOT EXISTS eslibre_monitor@'%' IDENTIFIED BY 'eslibre_monitor'"
docker exec $(basename $PWD)-mysql_galera_1-1 mysql -e "GRANT USAGE, REPLICATION CLIENT ON *.* TO eslibre_monitor@'%'"

printf "[$(date)] MySQL Provisioning COMPLETE!\n"
