#!/bin/bash

if [ ! $BOOTSTRAP ]; then
    mysqld
else
    echo "BOOTSTRAPING!"
    mysqld --wsrep-new-cluster
fi
