DELETE FROM mysql_servers;

INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (10, 'mysql_galera_1', 3306, "mysql_galera_1");
INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (20, 'mysql_galera_1', 3306, "mysql_galera_1");

INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (10, 'mysql_galera_2', 3306, "mysql_galera_2");
INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (30, 'mysql_galera_2', 3306, "mysql_galera_2");

INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (10, 'mysql_galera_3', 3306, "mysql_galera_3");
INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (30, 'mysql_galera_3', 3306, "mysql_galera_3");

LOAD MYSQL SERVERS TO RUNTIME;
SAVE MYSQL SERVERS TO DISK;
