# EXERCISE 0

DELETE FROM mysql_servers;

INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (10, 'mysql_galera_1', 3306, "mysql_galera_1");
INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (20, 'mysql_galera_1', 3306, "mysql_galera_1");

INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (10, 'mysql_galera_2', 3306, "mysql_galera_2");
INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (30, 'mysql_galera_2', 3306, "mysql_galera_2");

INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (10, 'mysql_galera_3', 3306, "mysql_galera_3");
INSERT INTO mysql_servers(hostgroup_id, hostname, port, comment) VALUES (30, 'mysql_galera_3', 3306, "mysql_galera_3");

LOAD MYSQL SERVERS TO RUNTIME;
SAVE MYSQL SERVERS TO DISK;

DELETE FROM mysql_query_rules;

# Firewall
INSERT INTO mysql_query_rules (rule_id, active, schemaname, username, match_pattern, error_msg, apply) VALUES (1001, 1, 'eslibre_admin', 'eslibre', '^INSERT\s+INTO.*', 'INSERT denied to "eslibre" user into "eslibre_admin"', 1);
INSERT INTO mysql_query_rules (rule_id, active, username, match_pattern, error_msg, apply) VALUES (1002, 1, 'eslibre', '^INSERT\s+INTO\s+eslibre_admin.*', 'INSERT denied to "eslibre" user into "eslibre_admin"', 1);

# Routing
INSERT INTO mysql_query_rules (rule_id, active, schemaname, match_pattern, destination_hostgroup, apply) VALUES (1010, 1, 'eslibre', '^SELECT.*FROM\s+Payments.*', 20, 1);
INSERT INTO mysql_query_rules (rule_id, active, match_pattern, destination_hostgroup, apply) VALUES (1020, 1, '^SELECT.*FROM\s+eslibre\.Payments.*', 20, 1);
INSERT INTO mysql_query_rules (rule_id, active, match_pattern, destination_hostgroup, apply) VALUES (1030, 1, '^SELECT.*FOR UPDATE', 20, 1);
INSERT INTO mysql_query_rules (rule_id, active, match_pattern, destination_hostgroup, apply) VALUES (1040, 1, '^SELECT.*', 10, 1);

## TO QUERY DATA
# SELECT rule_id, active, match_pattern, destination_hostgroup, apply FROM mysql_query_rules;

LOAD MYSQL QUERY RULES TO RUNTIME;
SAVE MYSQL QUERY RULES TO DISK;

DELETE FROM mysql_galera_hostgroups;
INSERT INTO mysql_galera_hostgroups (writer_hostgroup, backup_writer_hostgroup, reader_hostgroup, offline_hostgroup, active, max_writers, writer_is_also_reader, max_transactions_behind, comment) VALUES (20, 30, 10, 40, 1, 1, 1, 100, NULL);

LOAD MYSQL SERVERS TO RUNTIME;

LOAD SCHEDULER TO RUNTIME;
SAVE SCHEDULER TO DISK;

DELETE FROM mysql_users;
INSERT INTO mysql_users (username,password,active,default_hostgroup,transaction_persistent) values ('root','root',1,20,1);
INSERT INTO mysql_users (username,password,active,default_hostgroup,transaction_persistent) values ('eslibre','eslibre',1,20,1);
INSERT INTO mysql_users (username,password,active,default_hostgroup,transaction_persistent) values ('sbtest','sbtest',1,20,1);

LOAD MYSQL USERS TO RUNTIME;
SAVE MYSQL USERS TO DISK;

SET mysql-monitor_galera_healthcheck_interval=5000;
SET mysql-monitor_galera_healthcheck_timeout=1000;

SET mysql-monitor_username='eslibre_monitor';
SET mysql-monitor_password='eslibre_monitor';

LOAD MYSQL VARIABLES TO RUNTIME;
SAVE MYSQL VARIABLES TO DISK;

# EXERCISE 5
# MySQL 8: Clear text password are required tue to MYSQL 8 'caching_sha2_password'. Essentially the server
# is expected the 'hash', so we need to HASH the password before sending it to the server. So for creating
# the 'hash' we store the password in cleartext.

SET admin-hash_passwords='false';
LOAD ADMIN VARIABLES TO RUNTIME;
SAVE ADMIN VARIABLES TO DISK;

# RELOAD USERS
LOAD MYSQL USERS TO RUNTIME;
