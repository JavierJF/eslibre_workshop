DELETE FROM mysql_galera_hostgroups;
INSERT INTO mysql_galera_hostgroups (
    writer_hostgroup, backup_writer_hostgroup, reader_hostgroup, offline_hostgroup, active, max_writers,
    writer_is_also_reader, max_transactions_behind, comment
) VALUES (20, 30, 10, 40, 1, 1, 1, 100, NULL);

LOAD MYSQL SERVERS TO RUNTIME;
SAVE MYSQL SERVERS TO DISK;
