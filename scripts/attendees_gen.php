<?php

/**
 * Script to make galera consistency checks
*/
error_reporting(E_ALL ^ E_NOTICE);

function select_stmt($mysql, $sqlText) {
    $statement = $mysql->prepare($sqlText);
    $statement->execute();
    $results = $statement->get_result();
    
    $fetch_res = array();

    while($result = $results->fetch_assoc()) {
        $fetch_res[] = $result;
    }

    return $fetch_res;
}

function get_random_date($start, $end) {
    $timestamp = mt_rand($start, $end);
    return date("Y-m-d h-m-s", $timestamp);
}

function gen_rand_attendees($mysql, $num) {
    echo "Generating {$num} attendees and 10% of speakers...".PHP_EOL;

    $lastnames = file(dirname(__FILE__) . "/lastnames.txt");
    $surnames = file(dirname(__FILE__) . "/surnames.txt");
    $talk_titles = file(dirname(__FILE__) . "/awesome_titles.txt");

    $insert_attendee = 'INSERT INTO eslibre.Attendees (Name, DNI, Age) VALUES (?,?,?)';
    $stmt_attendee = $mysql->prepare($insert_attendee);

    $insert_speaker = 'INSERT INTO eslibre.Speakers (ID, TalkTitle, Date) VALUES (?,?,?)';
    $stmt_speaker = $mysql->prepare($insert_speaker);

    /* IDs of the added attendees */
    $attendees_ids = array();

    for ($i = 0; $i < $num; $i++) {
        $rnd_lastname = $lastnames[rand(0 , count($lastnames) -1)];
        $rnd_surname = $surnames[rand(0 , count($surnames) -1)];
        $rnd_talk_title = $talk_titles[rand(0 , count($talk_titles) -1)];

        $name = $rnd_lastname . " " . $rnd_surname;
        $name = str_replace(array("\r", "\n"), '', $name);
        $talk_title = str_replace(array("\r", "\n"), '', $rnd_talk_title);

        $ecode = -1;

        while ($ecode == -1 || $ecode == 1062) {
            try {
                $dni_num = rand(0, 99999999);
                $dni_str = sprintf("%08d", $dni_num) . range('A', 'Z')[rand(0, 25)];
                $age = rand(16, 80);

                $stmt_attendee->bind_param("ssi", $name, $dni_str, $age);
                $stmt_attendee->execute();

                $insert_id = mysqli_insert_id($mysql);
                array_push($attendees_ids, $insert_id);

                if (rand(0, 10) == 0) {
                    $start_date = strtotime("24 June 2022");
                    $end_date = strtotime("25 June 2022");
                    $date = get_random_date($start_date, $end_date);
 
                    $stmt_speaker->bind_param("iss", $insert_id, $talk_title, $date);
                    $stmt_speaker->execute();
                }

                $ecode = 0;
            } catch (mysqli_sql_exception $e) {
                $ecode = $e->getCode();

                if ($ecode == 1062) {
                    printf("Found duplicated entry. Retrying...");
                } else {
                    throw $e;
                }
            }
        }
    }

    return $attendees_ids;
}

function gen_attendees_payments($mysql, $attendees_ids, $payments_per_att) {
    echo "Adding attendees payments...".PHP_EOL;

    $insert_payment = 'INSERT INTO eslibre.Payments (AID, Ammount, Date) VALUES (?,?,?)';
    $stmt_payment = $mysql->prepare($insert_payment);
    
    // Add attendees payments
    foreach ($attendees_ids as $id) {
        for ($i = 0; $i < $payments_per_att; $i++) {
            $cur_date = date('d-m-y h:i:s');
            $ammount = 1.0;

            $stmt_payment->bind_param("ids", $id, $ammount, $cur_date);
            $stmt_payment->execute();
        }
    }
}

// Script example.php
$shortopts = "";
$longopts = array("populate:", "target:", "wsrep", "report_ids");

$options = getopt($shortopts, $longopts);
$req_tg_arg = array_key_exists("target", $options);

$f_req_opts = true;
foreach ($longopts as $opt) {
    if (strpos($opt, ":") != false && array_key_exists(substr($opt, 0, -1), $options) == false) {
        echo ":: Missing required argument '{$opt}'.".PHP_EOL;
        $f_req_opts = $f_req_opts && false;
    }
}

if ($f_req_opts === false) {
    exit(1);
}

$populate = $options["populate"];
$target = $options["target"];
$wsrep = array_key_exists("wsrep", $options);
$report_ids = array_key_exists("report_ids", $options);

// Connections
$username = "eslibre";
$password = "eslibre";

if ($target != "proxysql") {
    echo ":: Using direct GALERA primary and replica as target..".PHP_EOL;

    /* Primary */
    $galera = new mysqli("127.0.0.1", $username, $password, "eslibre", 13306);
    if ($galera->connect_errno) {
        die("Galercluster connect failed: " . $galera->connect_error);
    }

    /* Replica */
    $galera_2 = new mysqli("127.0.0.1", $username, $password, "eslibre", 13308);
    if ($galera_2->connect_errno) {
        die("Galercluster connect failed: " . $galera_2->connect_error);
    }
} else {
    echo ":: Using ProxySQL as target..".PHP_EOL;

    /* Primary */
    $galera = new mysqli("127.0.0.1", $username, $password, "eslibre", 6033);
    if ($galera->connect_errno) {
        die("Galercluster connect failed: " . $galera->connect_error);
    }

    /* Replica */
    $galera_2 = new mysqli("127.0.0.1", $username, $password, "eslibre", 6033);
    if ($galera_2->connect_errno) {
        die("Galercluster connect failed: " . $galera_2->connect_error);
    }
}


// Create the Attendees; check the indexes in the table
$attendees = 2000;
$attendees_ids = array();
$payments_ids = array();
$num_payments = 10;

if ($populate == 1) {
    echo ":: Performing cleanup and user generation...".PHP_EOL;

    // Cleanup all the tables information
    mysqli_query($galera, "DELETE FROM eslibre.Speakers");
    mysqli_query($galera, "DELETE FROM eslibre.Payments");
    mysqli_query($galera, "DELETE FROM eslibre.Attendees");

    sleep(1);

    $attendees_ids = gen_rand_attendees($galera, $attendees);
    gen_attendees_payments($galera, $attendees_ids, $num_payments);
} else {
    $result = $galera->query("SELECT ID FROM eslibre.Attendees");

    if ($result == false) {
        echo "Error or empty resultset: " . mysqli_error($galera).PHP_EOL;
        exit(1);
    }

    while ($row = mysqli_fetch_row($result)) {
        array_push($attendees_ids, $row[0]);
    }

    $result = $galera->query("SELECT ID FROM eslibre.Payments");

    if ($result == false) {
        echo "Error or empty resultset: " . mysqli_error($galera).PHP_EOL;
        exit(1);
    }

    while ($row = mysqli_fetch_row($result)) {
        array_push($payments_ids, $row[0]);
    }

    $galera->query("UPDATE eslibre.Payments SET Ammount=1");
    $sync = 0;

    while ($sync === 0) {
        $result = $galera_2->query("SELECT SUM(Ammount) FROM eslibre.Payments");
        $row = mysqli_fetch_row($result);

        if (intval($row[0]) === $num_payments*$attendees) {
            $sync = 1;
        } else {
            echo ":: Waiting for replication...".PHP_EOL;
            sleep(1);
        }
    }
}

// Update payments for generated attendees and select from replica
$update_payment = 'UPDATE eslibre.Payments SET Ammount = Ammount + 2 WHERE AID = ?';
$stmt_update = $galera->prepare($update_payment);

$select_payment = 'SELECT SUM(Ammount) FROM eslibre.Payments WHERE AID = ?';
$stmt_select = $galera_2->prepare($select_payment);

if ($wsrep === true) {
    echo ":: Setting SESSION 'wsrep_sync_wait'".PHP_EOL;
    $galera_2->query("SET SESSION wsrep_sync_wait=1");
}

$total_time = 0;

echo ":: Performing the payments updates...".PHP_EOL;
for ($i = 0; $i < 20; $i++) {
    $time_start = microtime(true);
    
    $mod_count = 0;
    $upd_payments_ids = array();

    foreach ($attendees_ids as $id) {
        if ($mod_count % 10 == 0) {
            $stmt_update->bind_param("i", $id);
            $stmt_update->execute();

            $stmt_select->bind_param("i", $id);
            $stmt_select->execute();
            $result = $stmt_select->get_result();

            while ($row = $result->fetch_array(MYSQLI_NUM)) {
                array_push($upd_payments_ids, array($id, $row[0]));
            }
        }
        $mod_count += 1;
    }

    // Display Script End time
    $time_end = microtime(true);

    $execution_time = $time_end - $time_start;
    $total_time += $execution_time;
}

echo "Elapsed Time: " . strval($total_time) . PHP_EOL;
echo "Updated ids: " . count($upd_payments_ids) . PHP_EOL;

if ($report_ids) {
    for ($i = 0; $i < count($upd_payments_ids); $i++) {
        if ($i % 10 == 0) {
            $id = $upd_payments_ids[$i][0];
            $val = $upd_payments_ids[$i][1];

            echo ":: id: {$id}, val: {$val}".PHP_EOL;
        }
    }
}

?>
