<?php

$url = 'https://clickbait.cbrgm.net/generate';
$data = array();

$num_titles = 100;
$recv_titles = array();

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);

$context  = stream_context_create($options);

for ($i = 0; $i < $num_titles; $i++) {
    $result = @file_get_contents($url, false, $context);
    if ($result === FALSE) {
         $error = error_get_last();
         echo "Oh no! Error: '" . $error['message'] . "'";
    } else {
        $headline = json_decode($result)->{"headline"};
        array_push($recv_titles, $headline);
    }
}

$titles_file = dirname(__FILE__) . "/awesome_titles_new.txt";
$titles_str = implode("\n", $recv_titles);

@file_put_contents($titles_file, $titles_str, LOCK_EX);

?>
